#!/usr/bin/make -f
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/GNUstep/debian/config.mk
d_app    := $(CURDIR)/debian/gorm.app
resdir   := $(GNUSTEP_SYSTEM_APPS)/Gorm.app/Resources
usrshare := /usr/share/GNUstep/Gorm.app
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--no-undefined -Wl,--as-needed \
				    -Wl,-rpath,/usr/lib/gorm.app
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
optim := debug=yes
endif

%:
	dh $@

override_dh_auto_build:
	dh_auto_build -- $(optim) messages=yes $(shell dpkg-buildflags --export=cmdline)
	convert Images/Gorm.tiff -resize 32x32 Gorm.xpm
	$(MAKE) -C Documentation

override_dh_clean:
	[ ! -f Documentation/Gorm.info ] || $(MAKE) -C Documentation distclean
	dh_clean Gorm.xpm

override_dh_auto_install:
# Cheat gnustep-make to install the libraries as private.
	$(MAKE) install GNUSTEP_SYSTEM_LIBRARIES=/usr/lib/gorm.app \
	  DESTDIR=$(CURDIR)/debian/tmp
# Reinstall libGorm at /usr/lib.  GormCore is also supposed to be a
# public library but no package is using it, so keep it out for now.
	$(RM) $(CURDIR)/debian/tmp/usr/lib/gorm.app/libGorm.so*
	$(MAKE) -C GormLib install DESTDIR=$(CURDIR)/debian/tmp
	$(MAKE) -C Documentation install DESTDIR=$(CURDIR)/debian/tmp

override_dh_link:
	gsdh_gnustep
	dh_installdirs $(usrshare)
	rm $(d_app)$(resdir)/*.desktop
# Move all images to /usr/share in compliance with the FHS.
# We can't move the whole of the app's Resources because the palettes
# are not arch-independent.
	for i in $(d_app)$(resdir)/*.tiff; do \
	  mv $$i $(d_app)$(usrshare) \
	  && dh_link $(usrshare)/$${i##*/} $(resdir)/$${i##*/}; \
	done
# Now move all of palettes' Resources.
	cd $(d_app)$(resdir); \
	  for i in *.palette/Resources; do \
	    mv $$i $(d_app)$(usrshare)/$${i%Resources}; \
	  done
	for i in $(d_app)$(usrshare)/*.palette; do \
	  dh_link $(usrshare)/$${i##*/} $(resdir)/$${i##*/}/Resources; \
	done
# The English.lproj directory contains only .gorm files, but some have
# images inside them.
	mv $(d_app)$(resdir)/English.lproj $(d_app)$(usrshare) \
	  && dh_link $(usrshare)/English.lproj $(resdir)/English.lproj
	chmod -x $(d_app)$(usrshare)/*.tiff

override_dh_strip:
	dh_strip --dbgsym-migration=gorm.app-dbg

override_dh_makeshlibs:
# Avoid lintian warnings for private libraries in gorm.app.
	dh_makeshlibs -pgorm.app -n
	dh_makeshlibs -plibgorm1 -V
